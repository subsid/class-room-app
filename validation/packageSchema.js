{
  "id": "/package",
  "type": "object",
  "required": ["name"],
  "properties": {
    "name": {"type": "string"},
    "base URL": {"type": "string"},
    "description": {
      "type": "object",
      "required": ["text", "lang"],
      "properties": {
        "text": { "type": "string" },
        "lang": { "type": "string" }
      }
    },
    "lessons": {
      "type": "array",
      "items": {
        "type": "object",
        "required": ["subject", "URL"],
        "properties": {
          "class": { "type": "integer" },
          "subject": { "type": "string" },
          "medium": { "type": "string" },
          "term": { "type": "string" },
          "lesson": { "type": "string" },
          "contentType": { "type": "string" },
          "description": {
            "type": "object",
            "properties": {
              "text": { "type": "string" },
              "lang": { "type": "string" }
            }
          },
          "URL": {"type": "string"}
        }
      }
    }
  }
}
