var fs = require('fs');
var arguments = process.argv.slice(2);

var lessonSchema = require('./lessonSchema.json');
var Validator = require('jsonschema').Validator;
var v = new Validator();

// Input file
var filePath = arguments[0];
var file = JSON.parse(fs.readFileSync(filePath, 'utf8'));

// Schema to use
var schemaPath = arguments[1];
var schema = JSON.parse(fs.readFileSync(schemaPath, 'utf8'));

v.addSchema(lessonSchema, '/lesson');
console.log(v.validate(file, schema));

