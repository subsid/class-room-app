var app = require('app');  // Module to control application life.
var BrowserWindow = require('browser-window');  // Module to create native browser window.
var ipc = require('ipc');
var fs = require('fs');
var path = require('path');

// Report crashes to our server.
require('crash-reporter').start();

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is GCed.
var mainWindow = null;

// Quit when all windows are closed.
app.on('window-all-closed', function() {
  // On OS X it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

// (function() {
//   switch (process.platform) {
//     case 'darwin':
//       app.commandLine.appendSwitch('ppapi-flash-path', path.normalize('/Applications/Google Chrome.app/Contents/Versions/45.0.2454.85/Google Chrome Framework.framework/Internet Plug-Ins/PepperFlash/PepperFlashPlayer.plugin'));
//       app.commandLine.appendSwitch('ppapi-flash-version', '18.0.0.232');
//       return;
//     default:
//       app.commandLine.appendSwitch('ppapi-flash-path', path.normalize('C:/Program Files (x86)/Google/Chrome/Application/44.0.2403.155/PepperFlash/pepflashplayer.dll'));
//       app.commandLine.appendSwitch('ppapi-flash-version', '18.0.0.232');
//       return;
//   }
// })();

ipc.on('appDir', function(event, arg) {
  event.returnValue = app.getDataPath();
})

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
app.on('ready', function() {
  var atomScreen = require('screen');
  var size = atomScreen.getPrimaryDisplay().workAreaSize;
  // Create the browser window.
  mainWindow = new BrowserWindow({
    height: size.height,
    width: size.width,
    'web-preferences': {
      plugins: true
    }
  });
  mainWindow.maximize();

  // and load the index.html of the app.
  mainWindow.loadUrl('file://' + __dirname + '/front/main.html');

  // Open the devtools.
  // mainWindow.openDevTools();

  // Emitted when the window is closed.
  mainWindow.on('closed', function() {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    mainWindow = null;
  });
});
