const React = require('react');
const T = React.PropTypes;

const StyleSheet = require('react-style');
const GroupedList = require('./grouped-list.jsx');

const styles = StyleSheet.create({
  active: {
    width: '20%',
  },
});

module.exports = React.createClass({
  displayName: 'Content Browser',

  propTypes: {
    configParser: T.object.isRequired,
    showContent: T.func.isRequired,
    online: T.bool.isRequired,
  },

  getInitialState: function() {
    return {
      filterBy: 'CLASS',
    };
  },

  componentDidMount: function() {
    $(this.refs.filterDropdown.getDOMNode())
    .dropdown({
      onChange: (val) => {
        if (this.state.filterBy !== val) {
          this.setState({
            filterBy: val,
          });
        }
      },
    })
    .dropdown('set selected', this.state.filterBy);
  },

  shouldComponentUpdate: function(nextProps, nextState) {
    if ((nextProps.online !== this.props.online) ||
                                   (nextState.filterBy !== this.state.filterBy)) {
      return true;
    }
    return false;
  },

  render: function() {
    return (
      <div styles={styles.active} className="ui content-browser left vertical sidebar menu">
        <div ref="filterDropdown" className="ui fluid selection dropdown">
          <input type="hidden" name="gender" />
          <i className="dropdown icon"></i>
          <div className="default text">Group by</div>
          <div className="menu">
            <div className="item" data-value="PACKAGE">Package</div>
            <div className="item" data-value="CLASS">Class</div>
          </div>
        </div>
        <GroupedList
          filterBy={this.state.filterBy}
          configParser={this.props.configParser}
          showContent={this.props.showContent}
          online={this.props.online}
        />
      </div>
    );
  },
});
