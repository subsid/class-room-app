const R = require('ramda');
const fs = require('fs');
const path = require('path');

module.exports = function(configDir) {
  const contentConfig = (() => {
    try {
      return JSON.parse(fs.readFileSync([configDir, 'config.json'].join(path.sep), 'utf8'));
    } catch (e) {
      console.log('oops');
      console.log(e);
    }
  })();

  // :: [PackageDirs]
  const packageDirs = (() => {
    const dirs = R.filter((name) => {
      return fs.statSync(configDir + path.sep + name).isDirectory();
    })(fs.readdirSync(configDir + path.sep + contentConfig.contentHome[0]));

    // dirs that contain package config file.
    return R.filter((dir) => {
      return fs.existsSync([configDir, dir, 'pkgConfig.json'].join(path.sep));
    })(dirs);
  })();

  // [Lessons] -> [Lessons]
  const addFileType = R.map((l) => {
    if (!l.fileType) {
      if (/https?:/.test(l.URL)) {
        return R.merge(l, {'fileType': 'url'});
      }
      return R.merge(l, {'fileType': path.extname(l.URL).substring(1)});
    }
    return l;
  });

  // :: [{packageConfig}]
  const packageConfigs = R.map((dir) => {
    const pc = JSON.parse(fs.readFileSync([configDir, dir, 'pkgConfig.json'].join(path.sep), 'utf8'));
    const lessonsWithFileType = addFileType(pc.lessons);
    return R.merge(pc, {'path': [configDir, dir].join(path.sep), 'lessons': lessonsWithFileType});
  })(packageDirs);

  const packageConfigsWithKey = R.map((p) => {
    return R.merge(p, {lessons: R.groupBy((l) => {
      const className = R.propOr('*', 'class')(l);
      const subject = R.toLower(R.propOr('*', 'subject')(l));
      const term = R.toLower(R.propOr('*', 'term')(l));
      const lesson = R.toLower(R.propOr('*', 'lesson')(l));

      return [className, subject, term, lesson].join('----');
    })(p.lessons)
  });
  })(packageConfigs);

  // :: [{Content}]
  function contentForClassSubjectTermLesson(c, s, t, l) {
    const contentByPackage = packageConfigsWithKey;
    return R.map((content) => {
      return R.merge(content, {lessons: (content.lessons[[c, s, t, l].join('----')] || [])});
    })(contentByPackage);
  }

  // :: {Class: {Term: {Subject: [Lesson]}}}
  const contentByClass = (function() {
    const contentsForClass = R.map((c) => {
      const className = c.class;
      const subject = c.subject;
      const term = c.term;
      const lessonsWithContents = R.map((lesson) => {
        return R.merge(lesson,
                       {contents: contentForClassSubjectTermLesson(className, subject.toLowerCase(), term.toLowerCase(), lesson.lesson.toLowerCase())});
      })(c.lessons);

      return R.merge(c, {lessons: lessonsWithContents});
    })(contentConfig.classlessons);

    // group lessons in config file by class-term-subject
    const groupedByClass = R.groupBy(R.prop('class'), contentsForClass);
    const groupedByClassAndTerm = R.mapObj(R.groupBy(R.prop('term')))(groupedByClass);
    const groupedByClassTermAndSubject = R.mapObj((c) => {
      return R.mapObj((t) => {
        return R.groupBy(R.prop('subject'))(t);
      })(c);
    })(groupedByClassAndTerm);
    return groupedByClassTermAndSubject;
  })();

  // {description: } -> [String, String]
  function getDescriptionAndClass(o) {
    const description = o.description;
    const getDescriptionText = function getDescriptionText(d) {
      try {
        return (decodeURIComponent(d.text));
      } catch(err) {
        return d.text;
      }
    };
    if (description) {
      if (description.lang.toLowerCase() === 'tamil') {
        return [getDescriptionText(description), 'tamil'];
      }
      return [getDescriptionText(description), ''];
    }
    return ['', ''];
  }

  // {lessonName: } -> [String, String]
  function getLessonNameAndClass(o) {
    const lessonName = o.lessonName;
    if (lessonName) {
      if (lessonName.lang.toLowerCase() === 'tamil') {
        return [lessonName.text, 'tamil'];
      }
      return [lessonName.text, ''];
    }
    return ['', ''];
  }

  function getFilePath(home, fileName) {
    switch (true) {
      case (/file:\//.test(fileName)):
        return [home, fileName.substr(6, fileName.length - 1)].join(path.sep);
      case (/https?:/.test(fileName)):
        return fileName;
      default:
        return [home, fileName].join(path.sep);
    }
  }

  function fileExists(filePath) {
    return fs.existsSync(decodeURIComponent(filePath));
  }

  return {
    fileExists,
    configDir,
    contentConfig,
    getFilePath,
    contentByClass,
    getDescriptionAndClass,
    getLessonNameAndClass,
    packageConfigs,
  };
};

