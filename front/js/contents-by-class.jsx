const React = require('react');
const R = require('ramda');
const T = React.PropTypes;
const StyleSheet = require('react-style');

const styles = StyleSheet.create({
  list: {
    marginTop: '1em',
  },
});

const ContentsByPackage = require('./contents-by-package.jsx');

module.exports = React.createClass({
  displayName: 'Content By Class',

  propTypes: {
    contents: T.object.isRequired,
    showContent: T.func.isRequired,
    configParser: T.object.isRequired,
    online: T.bool.isRequired,
  },

  componentDidMount: function() {
    $(this.refs['class-list-accordion'].getDOMNode()).accordion({
      exclusive: false,
    });
  },

  render: function() {

    // {Lesson} -> React.Node
    const getContentForLesson = (lesson) => {
      const filteredContents = R.filter((c) => c.lessons.length > 0)(lesson.contents);
      const lessonContent = R.ifElse(
        R.isEmpty,
        () => <div className="mini header">No Content</div>,
        (contents) => {
          return (
            <ContentsByPackage
              shouldInitializeAccordion={false}
              contents={contents}
              configParser={this.props.configParser}
              showContent={this.props.showContent}
              online={this.props.online}
            />
          );
        }
      );

      const [lessonName, lessonNameClass] = this.props.configParser.getLessonNameAndClass(lesson);

      return (
        <div key={[lesson.lesson, lesson.medium, lesson.lessonName.text].join('-')}>
          <div className={`title ${R.isEmpty(filteredContents) ? 'no-content' : ''}`}>
            <i className="dropdown icon"></i>
            {lesson.lesson + '. '}<span className={`${lessonNameClass}`}>{lessonName}</span>
          </div>
          <div className="content">
            {lessonContent(filteredContents)}
          </div>
        </div>
      );
    };


    // {Subject: [Lesson]} -> SubjectString -> React.Node
    const getContentForSubject = R.curry((contentsForClassTerm, subject) => {
      const contentsForClassTermSubject = contentsForClassTerm[subject];

      return (
        <div key={subject}>
          <div className="title" key={subject}>
            <i className="dropdown icon"></i>
            {subject}
          </div>
          <div className="content">
            <div className="ui styled accordion">
              {R.map(getContentForLesson, contentsForClassTermSubject[0].lessons)}
            </div>
          </div>
        </div>
      );
    });

    // :: {Term: {Subject: [Lesson]}} -> TermString -> React.Node
    const getContentForTerm = R.curry((contentsForClass, term) => {
      const contentsForClassTerm = contentsForClass[term];
      const subjectsForTerm = R.keys(contentsForClassTerm);

      return (
        <div key={term}>
          <div className="title">
            <i className="dropdown icon"></i>
            {term}
          </div>
          <div className="content">
            <div className="ui styled accordion">
              {R.map(getContentForSubject(contentsForClassTerm), subjectsForTerm)}
            </div>
          </div>
        </div>
      );
    });

    // {Class: {Term: {Subject: [Lesson]}}} -> ClassString -> React.Node
    const getClassItems = R.curry((contents, className) => {
      const contentsForClass = contents[className];
      const termsForClass = R.keys(contentsForClass);

      return (
        <div key={className}>
          <div className="title">
            <i className="folder icon">
            </i>
            {'Class ' + className}
          </div>
          <div className="content">
            <div className="ui styled accordion">
              {R.map(getContentForTerm(contentsForClass), termsForClass)}
            </div>
          </div>
        </div>
      );
    });

    return (
      <div ref="class-list-accordion" className="ui styled accordion" styles={styles.list} key={this.props.contents.name}>
        {R.map(getClassItems(this.props.contents))(R.keys(this.props.contents))}
      </div>
    );
  },
});
