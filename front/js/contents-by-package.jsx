const React = require('react');
const R = require('ramda');
const T = React.PropTypes;
const StyleSheet = require('react-style');

const styles = StyleSheet.create({
  list: {
    marginTop: '1em',
  },
});


module.exports = React.createClass({
  displayName: 'Content By Package',

  propTypes: {
    configParser: T.object.isRequired,
    shouldInitializeAccordion: T.bool,
    contents: T.array.isRequired,
    showContent: T.func.isRequired,
    online: T.bool.isRequired,
  },

  getDefaultProps: function() {
    return {
      shouldInitializeAccordion: true,
    };
  },

  componentDidMount: function() {
    // only initialize accordion when used outside of other accordions
    if (this.props.shouldInitializeAccordion) {
      $(this.refs['package-list-accordion'].getDOMNode()).accordion({
        exclusive: false,
      });
    }
  },

  getIcon: function(fileType) {
    switch(fileType.toLowerCase()) {
      case 'url':
        return (<i className={`ui icon ${this.props.online ? '' : 'red'} world `}></i>);
      case 'mp3':
        return (<i className="ui icon file audio outline"></i>);
      case 'mp4':
        return (<i className="ui icon file video outline"></i>);
      case 'avi':
        return (<i className="ui icon file video outline"></i>);
      case 'dvd':
        return (<i className="ui icon file video outline"></i>);
      case 'vcd':
        return (<i className="ui icon file video outline"></i>);
      case 'pdf':
        return (<i className="ui icon file pdf outline"></i>);
      case 'default':
        return (<i className="ui icon file outline"></i>);
    }
  },

  render: function() {
    const getLessonItem = R.curry((p, l) => {
      const [description, descriptionClass] = this.props.configParser.getDescriptionAndClass(l);
      return (
        <div key={[l.URL, l.description.text, l.class, l.subject, l.medium].join('-')} style={((l.fileType !== 'url') || (l.fileType === 'url' && this.props.online)) ? {} : {pointerEvents: 'none'}}>
          <a className="item" key={l.URL} onClick={this.props.showContent.bind(null, p.path, l)}>
            {this.getIcon(l.fileType)}
            <div className="content">
              <div className="header">{l.name}</div>
              <div className={`description ${descriptionClass}`}>
                  {description}
              </div>
            </div>
          </a>
        </div>
      );
    });

    const getPackageItem = (p) => {
      const name = p.name;
      const [description, descriptionClass] = this.props.configParser.getDescriptionAndClass(p);
      return (
        <div key={p.name}>
          <div className="title">
            <i className="folder icon">
            </i>
            {name}
          </div>
          <div className="content">
            <div className={`description ${descriptionClass}`}>
              {description}
            </div>
            <div className="ui list">
              {R.map(getLessonItem(p), p.lessons)}
            </div>
          </div>
        </div>
      );
    };

    return (
      <div ref="package-list-accordion" className="ui styled accordion" styles={styles.list} key={this.props.contents.name}>
        {R.map(getPackageItem, this.props.contents)}
      </div>
    );
  },
});
