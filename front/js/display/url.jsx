const React = require('react');
const T = React.PropTypes;
const R = require('ramda');

const styles = require('../styles.js');

module.exports = React.createClass({
  displayName: 'WebView',

  propTypes: {
    content: T.object,
    configParser: T.object.isRequired,
  },

  getInitialState: function() {
    return {
      isLoading: true
    };
  },

  componentDidMount: function() {
    const webview = this.refs['webview'].getDOMNode();
    $(webview).attr('plugins','');
    webview.addEventListener('did-stop-loading', () => {
      this.setState({isLoading: false});
    });
  },

  render: function() {
    return (
      <div className={`ui basic ${this.state.isLoading ? 'loading' : ''} segment`} styles={[styles.fullHeight, styles.centerAlign, styles.fullWidth]}>
        <webview ref="webview" src={this.props.configParser.getFilePath(this.props.content.home, this.props.content.lesson.URL)} styles={[styles.fullWidth, styles.fullHeight]}></webview>
      </div>
    );
  },
});
