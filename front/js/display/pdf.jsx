const React = require('react');
const T = React.PropTypes;
const styles = require('../styles.js');
const shell = require('shell');

module.exports = React.createClass({
  displayName: 'Launch PDF',

  propTypes: {
    content: T.object,
    configParser: T.object.isRequired,
  },

  componentWillReceiveProps: function(nextProps) {
    this.setState({
      launchExternal: false
    })
  },

  componentDidUpdate: function(prevProp, prevState) {
    if (this.refs.pdfIframe && !prevState.launchExternal) {
      this.refs.pdfIframe.getDOMNode().contentWindow.location.reload();
    }
  },

  getInitialState: function() {
    return {
      launchExternal: false
    }
  },

  openExternal: function() {
    this.setState({
      launchExternal: true
    })
  },

  render: function() {
    if (this.state.launchExternal) {
      shell.openItem(decodeURIComponent(this.props.configParser.getFilePath(this.props.content.home, this.props.content.lesson.URL)));
      return (
        <div>Launched file externally</div>
      );
    }

    return (
          <div>
            <div className="ui button" style={{marginBottom: '0.5em'}} onClick={this.openExternal}>Open External</div>
            <iframe ref="pdfIframe" src={
              `./vendor/pdfjs/web/viewer.html#${encodeURIComponent(this.props.configParser.getFilePath(this.props.content.home, this.props.content.lesson.URL))}`
            } styles={styles.pdfiframe} />
          </div>);
  }
});
