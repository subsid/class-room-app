const React = require('react');
const T = React.PropTypes;
const R = require('ramda');

const styles = require('../styles.js');

module.exports = React.createClass({
  displayName: 'Display Video',

  propTypes: {
    content: T.object,
    configParser: T.object.isRequired,
  },

  _playVideo: function() {
    $(this.refs.videotarget.getDOMNode())[0].play();
  },

  componentDidMount: function() {
    if (this.refs.videotarget) {
      this._playVideo();
    }
  },

  componentDidUpdate: function() {
    if (this.refs.videotarget) {
      $(this.refs.videotarget.getDOMNode())[0].load();
      this._playVideo();
    }
  },

  render: function() {
    return (
      R.ifElse(
        R.identity,
        () => {
          return (
            <div styles={[styles.fullHeight, styles.centerAlign]} ref="target">
              <video controls styles={[styles.video]} ref="videotarget">
                <source src={this.props.configParser.getFilePath(this.props.content.home, this.props.content.lesson.URL)} type="video/mp4" />
              </video>
            </div>);
        },
        () => {
          return (
            <div className="ui basic segment" styles={[styles.fullHeight, styles.centerAlign]}>
              File does not exist.
            </div>);
        }
      )(this.props.configParser.fileExists(this.props.configParser.getFilePath(this.props.content.home, this.props.content.lesson.URL)))
    );
  },
});
