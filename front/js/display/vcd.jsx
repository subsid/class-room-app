const React = require('react');
const T = React.PropTypes;
const spawn = require('child_process').spawn;
const path = require('path');

module.exports = React.createClass({
  displayName: 'Display VCD',

  propTypes: {
    content: T.object,
    configParser: T.object.isRequired,
  },

  render: function() {

    const childProces = (function() {
      switch (process.platform) {
        case 'darwin':
          return spawn('open', ['-a', 'vlc', '--args', '--quiet', path.normalize(decodeURIComponent(this.props.configParser.getFilePath(this.props.content.home, this.props.content.lesson.URL)))]);
        case 'win32':
          return spawn(path.normalize('C:/Program Files (x86)/Windows Media Player/wmplayer.exe'), [path.normalize(decodeURIComponent(this.props.configParser.getFilePath(this.props.content.home, this.props.content.lesson.URL)))]);
        default:
          return spawn(path.normalize('C:/Program Files (x86)/Windows Media Player/wmplayer.exe'), [path.normalize(decodeURIComponent(this.props.configParser.getFilePath(this.props.content.home, this.props.content.lesson.URL)))]);
      }
    }).bind(this)();

    return (
      <div>
        Launched file externally
      </div>
    );
  }
});
