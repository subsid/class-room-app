const React = require('react');
const T = React.PropTypes;
const R = require('ramda');

const styles = require('../styles.js');

module.exports = React.createClass({
  displayName: 'Display Audio',

  propTypes: {
    content: T.object,
    configParser: T.object.isRequired,
  },

  _playAudio: function() {
    $(this.refs.audiotarget.getDOMNode())[0].play();
  },

  componentDidMount: function() {
    if (this.refs.audiotarget) {
      this._playAudio();
    }
  },

  componentDidUpdate: function() {
    if (this.refs.audiotarget) {
      $(this.refs.audiotarget.getDOMNode())[0].load();
      this._playAudio();
    }
  },

  render: function() {
    return (
      R.ifElse(
        R.identity,
        () => {
          return (
            <div styles={[styles.fullHeight, styles.centerAlign]}>
              <audio controls ref="audiotarget">
                <source src={this.props.configParser.getFilePath(this.props.content.home, this.props.content.lesson.URL)} type="audio/mp3" />
              </audio>
            </div>);
        },
        () => {
          return (
            <div className="ui basic segment" styles={[styles.fullHeight, styles.centerAlign]}>
              File does not exist.
            </div>);
        }
      )(this.props.configParser.fileExists(this.props.configParser.getFilePath(this.props.content.home, this.props.content.lesson.URL)))
    );
  },
});
