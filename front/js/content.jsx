const React = require('react');
const R = require('ramda');
const T = React.PropTypes;

const ContentViewer = require('./content-viewer.jsx');
const ContentBrowser = require('./content-browser.jsx');
const SchoolSelector = require('./school-selector.jsx');
const {getSchools, getSelectedSchool, setSelectedSchool} = require('./usage-data.js');

const styles = require('./styles.js');

module.exports = React.createClass({
  propTypes: {
    configParser: T.object.isRequired,
    online: T.bool.isRequired,
    schoolName: T.string,
    schools: T.array,
    onSchoolSelect: T.func.isRequired,
  },

  getInitialState: function() {
    return ({
      contentToShow: null,
      showSchoolSelector: false,
    });
  },

  componentWillMount: function() {
    // If there are a list of schools, but no selectedschool
    // then prompt user to enter school.
    if (!R.isEmpty(this.props.schools) && R.isNil(this.props.schoolName)) {
      this.setState({
        showSchoolSelector: true,
        contentToShow: null
      });
    }
  },

  componentDidMount: function() {
    this.sidebar = $(this.refs['side-bar'].getDOMNode())
      this.sidebar.sidebar({
        context: $(this.refs['content-context'].getDOMNode()),
      })
      .sidebar('setting', 'transition', 'overlay')
      if (!this.state.showSchoolSelector) {
        this.sidebar.sidebar('toggle');
      }
  },

  componentWillReceiveProps: function(nextProps) {
    // If there are a list of schools, but no selectedschool
    // then prompt user to enter school.
    if (!R.isEmpty(nextProps.schools) && R.isNil(nextProps.schoolName)) {
      this.setState({
        showSchoolSelector: true,
        contentToShow: null
      });
    } else {
      if (this.state.showSchoolSelector) {
        this.setState({
          showSchoolSelector: false,
        });
      }
    }
  },

  _onSchoolSelect: function(school) {
    this.setState({
      showSchoolSelector: false,
      contentToShow: null
    }, () => this.props.onSchoolSelect(school));
  },

  showContent: function(dirPath, lesson) {
    this.toggleBrowser();
    this.setState({
      contentToShow: {
        home: dirPath,
        lesson: lesson,
      },
      showSchoolSelector: false,
    });
  },

  toggleBrowser: function() {
    $(this.refs['side-bar'].getDOMNode()).sidebar('toggle');
  },

  _showSchoolSelector: function() {
    this.sidebar.sidebar('hide');
    this.setState({
      showSchoolSelector: true,
      contentToShow: null
    });
  },

  _hideSelector: function() {
    this.setState({
      showSchoolSelector: false,
      contentToShow: null
    });
  },

  render: function() {
    const navbarSchoolName = (this.props.schoolName ? this.props.schoolName : "Select School" );
    const schoolSelectOrContent = () => {
      if (this.state.showSchoolSelector) {
        return (
          <SchoolSelector
            schools={this.props.schools}
            onSchoolSelect={this._onSchoolSelect}
            currentSchoolName={this.props.schoolName}
            show={this.state.showSchoolSelector}
            hideSelector={this._hideSelector}
          />
        );
      }
      return null;
    };

    return (
      <div className="main-page" styles={styles.fullHeight}>
        <div className="ui top attached demo menu" styles={styles.navbar}>
          <a className="item" onClick={this.toggleBrowser}>
            <i className="sidebar icon"></i>
            Select Content
          </a>
          <a className="item" onClick={this._showSchoolSelector}>{navbarSchoolName}</a>
          <div className="ui right menu">
            <img
              className="ui small image"
              src={"./Asha Logo Rectangle 1 Medium.jpg"}
              styles={styles.logo}
              width={"200px"}
              alt="Asha For Education"
            />
          </div>
        </div>
        <div ref="content-context" styles={styles.mainWindow}>
          <ContentBrowser
            showContent={this.showContent}
            configParser={this.props.configParser}
            online={this.props.online}
            ref="side-bar"/>
          <div className="pusher content-viewer">
            {schoolSelectOrContent()}
            <ContentViewer
              online={this.props.online}
              contentToShow={this.state.contentToShow}
              configParser={this.props.configParser}
              schoolName={this.props.schoolName}
            />
          </div>
        </div>
      </div>
    );
  },
});

