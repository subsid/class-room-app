const React = require('react');
const R = require('ramda');
const T = React.PropTypes;

const Content = require('./content.jsx');
const {getSchools, getSelectedSchool, setSelectedSchool, postUsageDataFromFile} = require('./usage-data.js');

const styles = require('./styles.js');

module.exports = React.createClass({
  propTypes: {
    configParser: T.object.isRequired,
  },

  getInitialState: function() {
    return ({
      schools: [],
      selectedSchool: null,
      online: window.navigator.onLine,
    });
  },

  _updateSchools: function() {
    getSchools((schools) => {
      this.setState({
        schools: schools,
      });
    });
  },

  componentWillMount: function() {
    this._updateSchools();
    postUsageDataFromFile();
    getSelectedSchool((school) => {
      this.setState({
        selectedSchool: school,
      });
    });
    setTimeout(() => {
      const onlineStatus = window.navigator.onLine;
      if (onlineStatus !== this.state.online) {
        this.setState({
          online: onlineStatus,
        });
      }
    }, 30000);
  },

  _onSchoolSelect: function(name) {
    setSelectedSchool(name);
    this.setState({
      selectedSchool: name,
    });
  },

  componentDidMount: function() {
    window.addEventListener('offline', () => {
      this.setState({
        online: false,
      });
    });

    window.addEventListener('online', () => {
      this.setState({
        online: true,
      });
    });
  },

  componentWillUpdate: function(nextProps, nextState) {
    if ((!this.state.online) && (nextState.online)) {
      this._updateSchools();
      postUsageDataFromFile();
    }
  },

  render: function() {
    return (
      <div styles={styles.fullHeight}>
        <Content
          online={this.state.online}
          configParser={this.props.configParser}
          schoolName={this.state.selectedSchool}
          schools={this.state.schools}
          onSchoolSelect={this._onSchoolSelect}
        />
      </div>
    );
  },
});

