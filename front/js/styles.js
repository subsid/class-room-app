const StyleSheet = require('react-style');
const atomScreen = require('screen');
const size = atomScreen.getPrimaryDisplay().workAreaSize;

const styles = StyleSheet.create({
  schoolSelectorModal: {
    position: "fixed",
    zIndex: 999,
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    margin: 0,
    padding: 0
  },

  modalBg: {
    position: "absolute",
    background: "rgba(0, 0, 0, 0.50)",
    top: 0,
    left: 0,
    bottom: 0,
    right: 0
  },

  modalForm: {
    position: "absolute",
    left: "50%",
    top: "50%",
    background: "#fefefe",
    width: "600px",
    marginLeft: "-20%",
    height: "225px",
    marginTop: "-13%"
  },

  selectSchool: {
    margin: "10%"
  },

  fullHeight: {
    height: "100%"
  },

  fullWidth: {
    width: "100%"
  },
  logo: {
    'float': 'right'
  },
  contentView: {
    height: (size.height - 200),
    width: (size.width - 300)
  },

  navbar: {
    height: "5%",
    maxHeight: "50px",
  },

  mainWindow: {
    height: "95%",
    overflow: "auto"
  },

  pdfiframe: {
    height: (size.height - 200),
    width: (size.width - 300)
  },

  video: {
    height: (size.height - 200),
    width: (size.width - 300)
  },

  webview: {
    height: (size.height - 200),
    width: (size.width - 300)
  },

  centerAlign: {
    textAlign: "center"
  }
});

module.exports = styles;
