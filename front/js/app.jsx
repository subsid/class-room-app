const path = require('path');
const fs = require('fs');

const React = require('react');
const Root = require('./root.jsx');
const R = require('ramda');

const configDir = (() => {
  switch (process.platform) {
    case 'darwin':
      return path.normalize('/Users/siddharth/workspace/asha-app/config');
    case 'win32':
      const folderNames = ['kanini', 'Kanini', 'KANINI'];
      const driveNames = ['C', 'D', 'E', 'F', 'G'];
      const possibleLocations = R.flatten(R.map((drive) => {
        return R.map((folder) => {
          return path.normalize(`${drive}:/${folder}`);
        })(folderNames);
      })(driveNames));

      return R.find((l) => {
        return fs.existsSync(l);
      })(possibleLocations);
    default:
      return path.normalize('C:/kanini');
  }
})();

const [canParse, configOrError] = (function() {
  try {
    const config = JSON.parse(fs.readFileSync([configDir, 'config.json'].join(path.sep), 'utf8'));
    return [true, config];
  } catch(err) {
    return [false, err];
  }
})();

if (canParse) {
  const configParser = require('./config-parser')(configDir, configOrError);
  require('../css/main.css');

  React.render(<Root
    configParser={configParser}
  />, document.getElementById('app'));
} else {
  const errMessage = `Cannot parse config file. ${configOrError}`;
  React.render(<div className="ui medium red header">{errMessage}</div>, document.getElementById('app'));
}
