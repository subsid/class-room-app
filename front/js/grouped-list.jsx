const React = require('react');
const R = require('ramda');
const T = React.PropTypes;

const ContentsByPackage = require('./contents-by-package.jsx');
const ContentsByClass = require('./contents-by-class.jsx');

module.exports = React.createClass({
  displayName: 'Grouped List',

  propTypes: {
    filterBy: T.string.isRequired,
    configParser: T.object.isRequired,
    showContent: T.func.isRequired,
    online: T.bool.isRequired,
  },

  render: function() {
    const contentsByPackage = this.props.configParser.packageConfigs;
    const contentsByClass = this.props.configParser.contentByClass;

    const groupedOutput = R.ifElse(
      () => (this.props.filterBy === 'CLASS'),
      () => {
        return (
          <ContentsByClass
            contents={contentsByClass}
            contentPackage={this.props.configParser}
            showContent={this.props.showContent}
            configParser={this.props.configParser}
            online={this.props.online}
          />
        );
      },
      () => {
        return (
          <ContentsByPackage
            contents={contentsByPackage}
            configParser={this.props.configParser}
            showContent={this.props.showContent}
            online={this.props.online}
          />
        );
      }
    )();

    return (
      <div>
        {groupedOutput}
      </div>
    );
  },
});
