// MAC:amac[curr],
// SchoolID:schoolinfo[0].SchoolID,
// Subject:asubject[curr],
// Class:aclass[curr],
// Term:aterm[curr],
// LessonNumber:alesson[curr],
// Medium:amedium[curr],
// PackageName:apackagename[curr],
// PackageItemURL:apackageitem[curr],
// Time:adt[curr],
// Data:adata[curr]
const R = require('ramda');
const fs = require('fs');
const path = require('path');
const ipc = require('ipc');
const currentDir = ipc.sendSync('appDir');
const {getMac} = require('getmac');
const endpoint = 'http://45.56.79.245';

function postUsageDataFromFile() {
  const online = window.navigator.onLine;
  if (online) {
    fs.readFile([currentDir, 'usageData.txt'].join(path.sep), 'utf-8', function(error, data) {
      if (error) {
      } else {
        $.post([endpoint, 'upload_usage'].join('/'), JSON.parse(data), function() {
          fs.unlinkSync([currentDir, 'usageData.txt'].join(path.sep));
        });
      }
    });
  }
}

function postUsageData(school, content, value) {
  const online = window.navigator.onLine;

  getMac(function(err, macAddress) {
    const usageStat = {
      school: school,
      mac: 'unknown',
      dt: new Date().toISOString().slice(0, 19).replace('T', ' '),
      classno: content.lesson['class'],
      subject: content.lesson.subject,
      term: content.lesson.term,
      lesson: content.lesson.lesson,
      medium: content.lesson.medium,
      packagename: content.home.split('/').pop(),
      packageitem: content.lesson.URL,
      data: value
    };

    if (err) {
      console.log('Unable to get macaddress');
    } else {
      const usageStatWithMac = R.merge(usageStat, {mac: macAddress});
      if (online) {
        $.post([endpoint, 'upload_usage'].join('/'), {data: [usageStatWithMac]});
      } else {
        // Make this more efficient
        fs.readFile([currentDir, 'usageData.txt'].join(path.sep), 'utf-8', function(error, data) {
          if (error) {
            fs.writeFileSync([currentDir, 'usageData.txt'].join(path.sep), JSON.stringify({data: [usageStatWithMac]}), 'utf-8');
          } else {
            const usageDataOld = JSON.parse(data);
            const updatedUsageData = R.merge(usageDataOld, {data: usageDataOld.data.concat(usageStatWithMac)});
            fs.writeFileSync([currentDir, 'usageData.txt'].join(path.sep), JSON.stringify(updatedUsageData), 'utf-8');
          }
        });
      }
    }
  });
}

function getSelectedSchool(cb) {
  fs.readFile([currentDir, 'selectedSchool.txt'].join(path.sep), 'utf-8', function(err, schoolName) {
    cb(schoolName);
  });
}

function setSelectedSchool(name) {
  fs.writeFile([currentDir, 'selectedSchool.txt'].join(path.sep), name);
}

function getSchools(cb) {
  const online = window.navigator.onLine;
  fs.readFile([currentDir, 'schools.txt'].join(path.sep), 'utf-8', function(err, data) {
    // If file does not exists, get schools if possible.
    if (err) {
      if (online) {
        $.get([endpoint, 'get_schools'].join('/'), function(schools) {
          fs.writeFile([currentDir, 'schools.txt'].join(path.sep), JSON.stringify(schools));
          cb(schools);
        });
      }
    // If file exists, update the file if possible
    } else {
      if (online) {
        $.get([endpoint, 'get_schools'].join('/'), function(schools) {
          fs.writeFile([currentDir, 'schools.txt'].join(path.sep), JSON.stringify(schools));
          cb(schools);
        });
      } else {
        cb(JSON.parse(data));
      }
    }
  });
}

module.exports = {
  setSelectedSchool,
  getSelectedSchool,
  postUsageData,
  postUsageDataFromFile,
  getSchools,
};
