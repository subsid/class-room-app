const React = require('react');
const T = React.PropTypes;
const R = require('ramda');
const shell = require('shell');

const styles = require('./styles.js');

const Video = require('./display/video.jsx');
const Vcd = require('./display/vcd.jsx');
const Audio = require('./display/audio.jsx');
const Pdf = require('./display/pdf.jsx');
const Url = require('./display/url.jsx');
const {postUsageData} = require('./usage-data.js');

module.exports = React.createClass({
  displayName: 'Content Viewer',

  propTypes: {
    contentToShow: T.object,
    configParser: T.object.isRequired,
    online: T.bool.isRequired,
    schoolName: T.string,
  },

  componentWillReceiveProps: function(nextProps) {
    // open package
    if (nextProps.contentToShow && !R.equals(nextProps.contentToShow, this.props.contentToShow)) {
      postUsageData(nextProps.schoolName, nextProps.contentToShow, 'PACKAGEOPEN');
    }
    // close package
    if (this.props.contentToShow && !R.equals(nextProps.contentToShow, this.props.contentToShow)) {
      postUsageData(this.props.schoolName, this.props.contentToShow, 'PACKAGECLOSE');
    }
  },

  render: function() {
    return (
      R.ifElse(
        R.isNil,
        () => {
          return (
            <div styles={styles.fullHeight}>
              <div className="ui basic default message segment" styles={[styles.centerAlign, styles.fullHeight]}>
                <div className="text">
                  <i className={"ui large icon chevron left"}></i>
                  <h3>Please Select Content</h3>
                </div>
              </div>
            </div>
          );
        },
        (content) => {
          const className = <h4 className="ui left floated header">Class {content.lesson.class}</h4>;
          const medium = <h4 className="ui left floated header">{content.lesson.medium} medium</h4>;
          const subject = <h4 className="ui left floated header">{content.lesson.subject}</h4>;
          const [description, descriptionClass] = this.props.configParser.getDescriptionAndClass(content.lesson);

          const renderedFile = ((fileType) => {
            switch (fileType.toLowerCase()) {
              case 'mp3':
                return ( <Audio
                  content={content}
                  configParser={this.props.configParser}
                />);
              case 'mp4':
                return ( <Video
                  content={content}
                  configParser={this.props.configParser}
                />);
              case 'pdf':
                return ( <Pdf
                  content={content}
                  configParser={this.props.configParser}
                />);
              case 'url':
                // Temporary fix till windows flash support is fixed
                shell.openExternal(decodeURIComponent(this.props.configParser.getFilePath(content.home, content.lesson.URL)));
                return (
                  <div>Launched file externally</div>
                );
                // return (
                //   R.ifElse(
                //     R.identity,
                //     () => {
                //       return (
                //         <Url
                //           content={content}
                //           configParser={this.props.configParser}
                //         />
                //       );
                //     },
                //     () => <div>No Network Connection</div>
                //   )(this.props.online)
                // );
              case 'dvd':
                shell.openItem(decodeURIComponent(this.props.configParser.getFilePath(content.home, content.lesson.URL)));
                return (
                  <div>Launched file externally</div>
                );
              case 'avi':
                shell.openItem(decodeURIComponent(this.props.configParser.getFilePath(content.home, content.lesson.URL)));
                return (
                  <div>Launched file externally</div>
                );
              case 'vcd':
                shell.openItem(decodeURIComponent(this.props.configParser.getFilePath(content.home, content.lesson.URL)));
                return (
                  <div>Launched file externally</div>
                );
              case 'ppt':
                shell.openItem(decodeURIComponent(this.props.configParser.getFilePath(content.home, content.lesson.URL)));
                return (
                  <div>Launched file externally</div>
                );
              case 'exe':
                shell.openItem(decodeURIComponent(this.props.configParser.getFilePath(content.home, content.lesson.URL)));
                return (
                  <div>Launched file externally</div>
                );
              case 'applet':
                return (shell.openItem(decodeURIComponent(this.props.configParser.getFilePath(content.home, content.lesson.URL))));
              default:
                return ( <div>Unsupported Content Type</div> );
            }
          })(content.lesson.fileType);

          return (
            <div className="ui centered grid container">
              <div className="row">
                {className}
                {medium}
                {subject}
              </div>
              <div className="row">
                <div className="ui header">
                  <h4 className={`ui ${descriptionClass} left floated header`}>{description}</h4>
                </div>
              </div>
              <div className="row" styles={[styles.contentView]}>
                {renderedFile}
              </div>
            </div>
          );
        }
      )(this.props.contentToShow)
    );
  },
});
