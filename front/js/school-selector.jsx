const React = require('react');
const R = require('ramda');
const T = React.PropTypes;

const styles = require('./styles.js');

module.exports = React.createClass({

  propTypes: {
    currentSchoolName: T.string,
    schools: T.array.isRequired,
    onSchoolSelect: T.func.isRequired,
    hideSelector: T.func.isRequired,
  },

  getInitialState: function() {
    return {
      schoolName: null
    }
  },

  componentDidMount: function() {
    this.dropdownNode = $(this.refs.dropdown.getDOMNode());
    this.dropdownNode.dropdown({
      fullTextSearch: true,
      onChange: (id, schoolName) => {
        this.setState({
          schoolName: schoolName,
        });
      }
    });
    (this.props.currentSchoolName ? this.dropdownNode.dropdown('set selected', this.props.currentSchoolName) : '');
  },

  componentWillReceiveProps: function(nextProps) {
    if (nextProps.currentSchoolName) {
      this.dropdownNode.dropdown('set selected', nextProps.currentSchoolName);
    }
  },

  _updateSchool: function() {
    this.props.onSchoolSelect(this.state.schoolName);
  },

  render: function() {
    const getOption = function(s) {
      return (
        <div className="item" data-value={s.SchoolName} key={s.SchoolID}>{s.SchoolName}</div>
      );
    };

    return (
      <div styles={styles.navbar}>
        <form className={`ui school-selector-form form ${R.isEmpty(this.props.schools) ? 'error' : ''}`}>
          <div className="inline field" style={{marginLeft: '1em'}}>
            <label>Select School</label>
            <div ref="dropdown" className="ui inline search selection dropdown">
              <input type="hidden" name="school" />
              <i className="dropdown icon" style={{margin: '-.7em'}}></i>
              <div className="default text">School</div>
              <div className="menu">
                {R.map(getOption)(this.props.schools)}
              </div>
            </div>
            <div className={`ui primary submit tiny button ${(this.state.schoolName === this.props.currentSchoolName) ? 'disabled' : ''}`}
              style={{marginLeft: '1em'}}
              onClick={this._updateSchool}>Submit</div>
            <div className="ui cancel tiny button" onClick={this.props.hideSelector}>Cancel</div>
          </div>
        </form>
      </div>
    );
  }
});

