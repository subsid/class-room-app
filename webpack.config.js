module.exports = {
  target: 'atom',
  entry: './front/js/app.jsx',
    output: {
        path: __dirname + '/front/',
        filename: 'bundle.js',
    },
    module: {
      loaders: [
        {
          test: /\.jsx?$/,
          exclude: /(node_modules)/,
          loader: 'babel-loader?stage=0',
        },
        {
          test: /\.css$/,
          loader: 'style!css!',
        },
        {
          test: /\.jpe?g$|\.gif$|\.png$|\.svg$|\.woff2?$|\.ttf$|\.wav$|\.mp3$/,
          loader: 'file',
        },
        {
          test: /\.json$/,
          loader: 'json',
        },
      ],
    },
};
